﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuizController : MonoBehaviour
{
    [SerializeField]
    private QuizButton quizButton;

    [SerializeField]
    private Transform buttonParent;
    private void Start()
    {
        for (int i =0; i<5; i++)
        { 
            // создаем кнопку внутри buttonParent
            var button = Instantiate(quizButton, buttonParent);
            button.Init(i, "Question " + i);
            button.onClick += ButtonPress; // Должен быть Int как в <int>
        }
    }

    private void ButtonPress(int id)
    {
        Debug.Log("Button pressed id " + id);
    }
}
