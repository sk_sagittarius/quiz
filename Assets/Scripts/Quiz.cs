﻿using System;
using System.IO;
using UnityEngine;
using UnityEngine.UI;


public class Quiz : MonoBehaviour
{
    [SerializeField]
    private Text question;
    int score = 0; // обнуляю счет

    [SerializeField]
    private Button trueButton, falseButton;
    private int counter = 0;
    private int count;
    private QuizQuestions activeQuestion;
    private QuizQuestions[] quizList;

    [SerializeField]
    private GameObject Statistics;
    [SerializeField]
    private Text resultPersent, questionCount, rightAnswers;

    private void Start()
    {
        quizList = GetQuizModel(); // выгружаю в лист данные
        count = quizList.Length;
        
        
        ShowQuestion(quizList); // вынесла код в отдельную функцию
        
        trueButton.onClick.AddListener(delegate{CheckResult(true);}); // listener на true
        falseButton.onClick.AddListener(delegate{CheckResult(false);}); // listener на false
        Statistics.gameObject.SetActive(false);
        
    }

    private void ShowQuestion(QuizQuestions[] quizList) // передаю лист
    {
        Debug.Log(count);
        Debug.Log(counter);
        if(counter < count)
        {
            activeQuestion = quizList[counter];
            question.text = activeQuestion.name_ru;
            counter++;
        }
        else
        {
            Statistics.gameObject.SetActive(true);
            var result = (100 * score) / count; 
            resultPersent.text = "Результат: " + result.ToString();
            questionCount.text = count.ToString();
            rightAnswers.text = score.ToString();
        }
        //return activeQuestion;


    }



    private void CheckResult(bool answ)
    {
        if(activeQuestion.right_answer == answ)
        {
            score++;
        }
        Debug.Log("score " + score);
        ShowQuestion(quizList);
    }


    private QuizQuestions[] GetQuizModel()
    {
        string path = Application.streamingAssetsPath +"/Database/Questions.json";
        string jsonString = File.ReadAllText(path); 
        QuizQuestions[] questions = JsonHelper.getJsonArray<QuizQuestions>(jsonString);
        //Debug.Log("COUNT " + questions.Length);
        return questions;
    }

    [Serializable]
    public class QuizQuestions
    {
        public int id;
        public string name_ru;
        public bool right_answer;
    }



    public class JsonHelper
    {
        public static T[] getJsonArray<T>(string json)
        {
            string newJson = "{ \"array\": " + json + "}";
            Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>> (newJson);
            return wrapper.array;
        }
    
        [System.Serializable]
        private class Wrapper<T>
        {
            public T[] array;
        }
    }
}
